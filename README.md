Adwaita Soda (Gnome 3 sublime text theme)
====
This is sublime text theme based on Soda to match the Gnome 3 (Adwaita Theme)

Screenshot
====
![Screenshot](https://bitbucket.org/johonunu/soda-adwaita/raw/d042cb734819ce65fcb55f4748772a8ba285e96b/adwaita.png)

How to install
====
- Go to your directory: ~/.config/sublime-text-2/Packages
- Move "Theme - Soda" folder in it
- Open Sublime Text
- Go to Preferences -> Settings - Default
- Go to line 246 ("theme": "Default.sublime-theme") and replace it with this:

"theme": "Soda Adwaita.sublime-theme",

"soda_classic_tabs": true,
